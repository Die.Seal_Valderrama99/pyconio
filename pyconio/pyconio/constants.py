# This file is part of the PyConio module.
# Copyright 2018 - konniskatt
# ASCII control characters constants

ACK = "\x06"  # Acknowledge
BEL = "\x07"  # Bell
BS = "\x08"  # Backspace
CAN = "\x18"  # Cancel
CR = "\x0d"  # Carriage return
DC1 = "\x11"  # Device control No. 1
DC2 = "\x12"  # Device control No. 2
DC3 = "\x13"  # Device control No. 3
DC4 = "\x14"  # Device control No. 4
DEL = "\x7f"  # Delete
DLE = "\x10"  # Data link escape
EM = "\x19"  # End of medium
ENQ = "\x05"  # Enquiry
EOT = "\x04"  # End of transmission
ESC = "\x1b"  # Escape
ETB = "\x17"  # End of transmission block
FF = "\x0c"  # Form feed
FS = "\x1c"  # File separator
GS = "\x1d"  # Group separator
LF = "\x0a"  # Linefeed
NAK = "\x15"  # Negative acknowledge
NUL = "\x00"  # Null
HT = "\x09"  # Horizontal Tab.
RS = "\x1e"  # Record separator
SI = "\x0f"  # Shift in
SO = "\x0e"  # Shift out
SOH = "\x01"  # Start of header
STX = "\x02"  # Start of text
SYN = "\x16"  # Synchronous idle
SUB = "\x1a"  # Substitute
SP = "\x20"  # Space
US = "\x1f"  # Unit separator
VT = "\x0b"  # Vertical tab.
XON = "\x11"  # Terminal allowed to resume transmitting
XOFF = "\x13"  # Terminal must pause and refrain from transmitting
