## **PyConio**
PyConio is a module that provides useful functions to work with console I/O.

All the **PyConio** functions have the same behaviour of conio.h.
And best of all, is **Cross-Platform!**

This module is under the GNU GPLv2 license. See LICENSE to more details.


## Installing
Execute this at your command prompt:

    pip3 install pyconio
in administrator or sudo mode depending of your os.

*From source:*

Go to dist folder and there is a tarball file (pyconio_x.x.tar.gz):
Extract the folder that is inside in the tarball (.tar.gz), and then execute: 

    python3 setup.py install 
in administrator or sudo mode depending of your os.

## Instalación (spanish)
Ejecuta esto en la consola:

    pip3 install pyconio
en modo administrador o sudo, dependiendo de tu sistema operativo.

*Desde fuente:*

Ve a la carpeta dist y ahi hay un archivo tarball (pyconio_x.x.tar.gz):
Extrae la carpeta que esta dentro del tarball (.tar.gz), y después ejecuta:

    python3 setup.py install
en modo administrador o sudo, dependiendo de tu sistema operativo.


## Contributing
See **CONTRIBUTING.md** to contributing details.

## **Documentation**
![colors](https://raw.githubusercontent.com/konniskatt/konniskatt.github.io/master/pyconio_colorswin.png)
![colors2](https://raw.githubusercontent.com/konniskatt/konniskatt.github.io/master/pyconio_ubuntu.png)
## Coloring text (4 Bits color palette, 16 colors)

    from pyconio import *
    textcolor(Red)
    print("This text is in red!")
    textbackground(Green) # Fore color maintains
    print("This is a red text with green background!")
    

    from pyconio import *
    textcolor(Red)
    textbackground(Green)
    print("spam")
    reversevideo()
    # Now fore is green and background is Red
    normvideo()
    # Resets console to defaults

*4 Bits color palette list:*
**Black, Blue, Green, Red, Yellow, Cyan, Magenta, White, Gray, LightRed, LightGreen, LightBlue, LightWhite, LightYellow, LightCyan and LightMagenta.**

# Coloring text (8 Bits extended palette, 256 colors)
    from pyconio import *

    textcolor(69, 8)
    print("Here")
    textcolor(34, 8)
    print("using")
    textcolor(208, 8)
    print("the")
    textcolor(196, 8)
    print("256")
    textcolor(226, 8)
    print("color")
    textcolor(87, 8)
    print("palette!")

![256](https://raw.githubusercontent.com/konniskatt/konniskatt.github.io/master/pyconio_256.png)
You can find more info [here](https://en.wikipedia.org/wiki/ANSI_escape_code).

# Coloring text (24 Bits True RGB palette, 16,777,216 colors)
    from pyconio import *
    import colorsys as clsy
        for i in range(256):
        color = clsy.hsv_to_rgb(i / 255, 1, 1)
        color = int(color[0] * 255), int(color[1] * 255), int(color[2] * 255)
        rgb_backgroundcolor(color[0], color[1], color[2])

    print()
    pause()


Note: When the program finishes, it reset colors.
## Controlling cursor position

    from pyconio import *
    gotoxy(5, 5)
    print("The cursor is now on x:5 and y:5")

## Read keyboard
	from pyconio import *
	c = getch()
	print("You pressed: %s" % c)

**getch():** 
Reads a key from keyboard and returns the pressed key.

**getche():**
Reads a key from keyboard, returns the pressed key and echoes it.

**getchar():**
Reads one character from keyboard and returns the pressed key, but waits until user press enter.

**kbhit():**
Detects if a key was pressed, and returns True if was a key pressed.

## Micellaneous functions
**Setting a title to console:**

    from pyconio import *
    title("eggs")

**Pause program execution:**

    from pyconio import *
    pause()
    # Output: Press any key to continue . . .
    # You can print your own message
    pause("The program is paused")
    # Output: This program is paused

**Hiding cursor and showing cursor:**

    from pyconio import *
    hidecur() # The cursor is now hidden
    showcur() # Now the cursor is not hidden

**Cleaning screen:**

    from pyconio import *
    clrscr()

**Get terminal size:**
    
    from pyconio import *
    a = gettermsz()

*Returns:* tuple with X and Y terminal size.

## Examples
A *(very)* simple game using PyConio
![game](https://raw.githubusercontent.com/konniskatt/konniskatt.github.io/master/pyconio_demo.png)


## TODO:
1. Add a cross-platform function like `getch()`, but that also detects control keys (Ctrl, Shift, Tab, Backspace, F1..F12)
