## Contributing
If you have any problems or a suggestion for the project, just open a [issue](https://gitlab.com/konniskatt/pyconio/issues).

Also you can make a merge request.

Your submitted code must follow the PEP-8 guidelines

Your code will be reviewed by me (konniskatt), and will be accepted if it is of quality. 

On the other hand, if it is rejected, you will be contacted to tell you the faults of your code.